function countLetter(letter, sentence) {
    
    // Check first whether the letter is a single character.

    //Conditons:
        // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        // If letter is invalid, return undefined.

        if(letter.length === 1){
            let result = 0;
            for (let i = 0; i < sentence.length; i++) {
                const letterInSentence = sentence[i];
                if(letterInSentence == letter){
                    result++
                }
            }
            return result
        }else{
            return undefined
        }
}


function isIsogram(text) {
    //Goal:
        // An isogram is a word where there are no repeating letters.

    //Check:
        // The function should disregard text casing before doing anything else.

    //Condition:
        // If the function finds a repeating letter, return false. Otherwise, return true.
        
        const lowerCaseText = text.toLowerCase()
        for (let i = 0; i < lowerCaseText.length; i++) {
            for (let j = 0; j < lowerCaseText.length; j++) {
                if(i!=j){
                    if (lowerCaseText[i] == lowerCaseText[j]){
                        return false
                    }
                }
            }
        }
        return true 
}

function purchase(age, price) {

    //Conditions:
        // Return undefined for people aged below 13.
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        // Return the rounded off price for people aged 22 to 64.

    //Check:
        // The returned value should be a string.

        if(age < 13){
            return undefined
        }else if(age >= 13 && age <= 21 || age > 60){
            return (price * 0.8).toFixed(2)
        }else{
            return price.toFixed(2)
        }
    
}

function findHotCategories(items) {
    //Goal:
        // Find categories that has no more stocks.
        // The hot categories must be unique; no repeating categories.

    //Array for the test:
        // The passed items array from the test are the following:
        // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
        // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
        // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
        // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
        // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }


    //Expected return must be array:
        // The expected output after processing the items array is ['toiletries', 'gadgets'].

    // Note:
        // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
        let hotItems = items.filter(item => item.stocks === 0)
        let hotItemsCategories = hotItems.map(item => item.category )

        let hotItemsCategory = hotItemsCategories.filter((item, index) => hotItemsCategories.indexOf(item) == index)

        return hotItemsCategory

}

function findFlyingVoters(candidateA, candidateB) {
    //Goal:
        // Find voters who voted for both candidate A and candidate B.

    //Array for the test:    
        // The passed values from the test are the following:
        // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
        // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // Expected return must be array:
        // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    //Note:
        // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

        voters = []
        for (let i = 0; i < candidateA.length; i++) {
            for (let j = 0; j < candidateB.length; j++) {
                if(i!=j){
                    if (candidateA[i] == candidateB[j]){
                        voters.push(candidateA[i])
                        
                    }
                }
            }
        }
        return voters
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};